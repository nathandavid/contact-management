import { Meteor } from 'meteor/meteor';
import Vue from 'vue';
import VueMeteorTracker from 'vue-meteor-tracker';

import App from '/imports/ui/App.vue';
import router from './routes';
import store from '/imports/store';

Vue.use(VueMeteorTracker);

Meteor.startup(() => {
    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('app');
});

import { Meteor } from 'meteor/meteor';

// Deny all client-side queries against the Users collection
Meteor.users.deny({
	insert() { return true; },
	update() { return true; },
	remove() { return true; }
});

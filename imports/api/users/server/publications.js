import { Meteor } from 'meteor/meteor';

Meteor.publish('usersAll', function () {
	return Meteor.users.find();
});

Meteor.publish('usersAdmin', function () {
	return Meteor.users.find({ admin: true }, {
		fields: {
			admin: 1
		}
	});
});

Meteor.publish('userProfile', function (userId) {
	return Meteor.users.find(userId);
});

// Meteor.startup(() => {
// 	Meteor.users.createIndex({ first_name: 1 });
// });

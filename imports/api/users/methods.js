import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';

export const updateUser = new ValidatedMethod({
	name: 'user.update',

	validate: new SimpleSchema({
		id: { type: String },
		first_name: { type: String },
		last_name: { type: String },
		status: { type: String },
		role: { type: String },
		work_title: { type: String },
		department: { type: String },
		location: { type: String },
		remote: { type: Boolean },
		country: { type: String },
		state: { type: String },
		city: { type: String },
		street: { type: String },
		postal: { type: String },
		apt_number: { type: String },
		photo: { type: String },
		company_mobile: { type: String },
		company_phone: { type: String },
		company_phone_ext: { type: String },
		personal_mobile: { type: String },
		personal_home: { type: String },
		company_email: { type: String },
		personal_email: { type: String },
		emg_first_name: { type: String },
		emg_last_name: { type: String },
		emg_relationship: { type: String },
		emg_work_phone: { type: String },
		emg_work_phone_ext: { type: String },
		emg_mobile_phone: { type: String },
		emg_home_phone: { type: String },
		emg_work_email: { type: String },
		emg_personal_email: { type: String }
	}).validator(),

	run(userObj) {
		Meteor.users.update(userObj.id, { $set: userObj });
	}
});

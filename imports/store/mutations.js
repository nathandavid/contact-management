const mutations = {
    showAdminLogin(state) {
        state.adminLogin = true;
    },
    hideAdminLogin(state) {
        state.adminLogin = false;
    },
    updateAccessToken(state, token) {
        state.accessToken = token;
    }
};

export default mutations;

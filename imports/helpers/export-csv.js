import { Meteor } from 'meteor/meteor';
import { CSV } from 'meteor/clinical:csv';
import FileReader from 'filereader';

Meteor.methods({
    exportCSV() {
        return CSV.unparse(Meteor.users.find().fetch());
    },
    importCSV() {
        const fileReader = new FileReader();
        fileReader.onload = function (e) {
            var jsonRepresentationOfCsv = CSV.parse(fileReader.result);
            console.log('jsonRepresentationOfCsv', jsonRepresentationOfCsv);
            // load into DB
        };
        fileReader.onerror = function (e) {
            throw 'Error reading CSV file';
        };

        fileReader.readAsText(file);
    }
});

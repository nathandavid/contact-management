/*
    Returns array of objects with appropriate schema
    @param JSON - receives JSON file
*/
export default (jsonFile) => {
    let transform = [];

    jsonFile.map((i) => {
        let obj = {};
        
        for (key in i) {
            switch (key) {
                case 'Email':
                    obj['email'] = i[key];
                    break;
                case 'First Name':
                    obj['first_name'] = i[key];
                    break;
                case 'Last Name':
                    obj['last_name'] = i[key];
                    break;
                case 'Emp Status':
                    obj['status'] = i[key];
                    break;
                case 'Job Title':
                    obj['work_title'] = i[key];
                    break;
                case 'DEPT ID':
                    obj['department'] = i[key];
                    break;
                case 'Office':
                    obj['location'] = i[key];
                    break;
                case 'Work From Home':
                    obj['remote'] = i[key];
                    break;
                case 'Location':
                    obj['country'] = i[key];
                    break;
                case 'Mobile':
                    obj['company_mobile'] = i[key];
                    break;
                case 'Phone':
                    obj['company_phone'] = i[key];
                    break;
                case 'EXT':
                    obj['company_phone_ext'] = i[key];
                    break;
                default:
                    break;
            }
        }
        
        if (Object.keys(obj).length) {
            transform.push(obj);
        }
    });

    return transform;
}

import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

export default () => {
    return new Promise((resolve, reject) => {
        Tracker.autorun((c) => {
            // stop computation when Meteor.user() is ready
            Meteor.user().admin !== undefined && c.stop();
            // return false if user is a guest
            Meteor.user().admin === null && resolve(false);
            // return true if user is logged in
            Meteor.user().admin && resolve(true);
        });
    });
};

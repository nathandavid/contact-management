const subsCache = new SubsCache({
    expireAfter: 60,
    cacheLimit: -1
});
  
Vue.config.meteor.subscribe = function(...args) {
    return subsCache.subscribe(...args);
};

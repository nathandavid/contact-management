import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods({
    findByEmail(email) {
        return Accounts.findUserByEmail(email);
    }
});

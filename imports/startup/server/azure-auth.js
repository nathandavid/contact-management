import { Meteor } from 'meteor/meteor';
import adal from 'adal-node';

const AuthenticationContext = adal.AuthenticationContext;
const clientId = process.env.AZURE_CLIENT_ID;
const clientSecret = process.env.AZURE_CLIENT_SECRET;
const authorityHostUrl = 'https://login.windows.net';
const tenant = '';
const authorityUrl = `${authorityHostUrl}/${tenant}`;
const redirectUri = process.env.ROOT_URL;
const resource = '00000002-0000-0000-c000-000000000000';

Meteor.methods({
	getAccessToken(code) {
		const authenticationContext = new AuthenticationContext(authorityUrl);

		const token = new Promise((resolve, reject) => {
			authenticationContext.acquireTokenWithAuthorizationCode(
				code,
				redirectUri,
				resource,
				clientId, 
				clientSecret,
				(err, res) => {
					if (err) reject(err)
					else resolve(JSON.stringify(res));
				}
			);
		})

		return token.then((success) => {
			return success;
		});
	}
});
